Generates a chart for each file (in a specified directory) that shows 12 columns (months), where each column represents the number of files that have the said month in their creationTime | lastModifiedTime. Also one chart of totals will be generated.

#### Run with:

    $ mvn compile exec:exec -Dargument1=<timeField> -Dargument2=<path>
    
    timeField  =  creationTime | lastModifiedTime
    path       =  "/path to/directory/"

Observe the generated charts in the `./CHARTS/` directory  
*(The directory will be created if it doesn't exist. It won't be cleared, but conflicting files will be overwritten).*


*Example chart:*

![](https://raw.githubusercontent.com/gima/filetimechart/master/misc/examplechart.png)


#### Generate Eclipse .project files

    mvn eclipse:eclipse
