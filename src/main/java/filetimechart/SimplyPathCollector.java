package filetimechart;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;

class SimplyPathCollector implements FileVisitor<Path> {
  
  LinkedList<PathAndAttrs> paths;
  
  public SimplyPathCollector() {
    paths = new LinkedList<>();
    
  }

  @Override
  public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
    if (Files.isRegularFile(path)) {
      paths.add(new PathAndAttrs(path, attrs));
    }
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
    throw exc;
  }

  @Override
  public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
    if (exc != null) throw exc;
    return FileVisitResult.CONTINUE;
  }
  
}