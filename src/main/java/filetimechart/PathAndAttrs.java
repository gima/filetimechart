package filetimechart;

import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

class PathAndAttrs {
  Path path;
  BasicFileAttributes attrs;
  public PathAndAttrs(Path path, BasicFileAttributes attrs) {
    this.path = path;
    this.attrs = attrs;
  }
}