package filetimechart;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class BufferedImageJFrame extends JFrame {
  public BufferedImageJFrame(String title, final BufferedImage bi) {
    super(title);
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    JPanel panel = new JPanel() {
      @Override
      public void paint(Graphics g) {
        g.drawImage(bi, 0, 0, null);
      }
    };
    add(panel);
    panel.setPreferredSize(new Dimension(bi.getWidth(), bi.getHeight()));

    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  public static void showImage(final String title, final BufferedImage bi) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        new BufferedImageJFrame(title, bi);
      }
    });
  }

}