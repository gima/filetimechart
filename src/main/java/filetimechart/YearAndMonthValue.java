package filetimechart;

public class YearAndMonthValue {
  int year;
  int[] monthValues;
  public YearAndMonthValue(int year, int[] monthValues) {
    this.year = year;
    this.monthValues = monthValues;
  }
}
