package filetimechart;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.joda.time.DateTime;

/*
 * old draft:
# collect data from files
  # for all files: linkedlist (sorted by date) 
# display data per year
  # for each column-to-be-displayed
    # loop from IDX:CURRENT to (UNTIL:OUTOFCOLUMN'S-DATE or end of files)
      # collect numerical amount
    # assign chart column value
  # display/save resulting chart
# display averaged for all years
  # loop each column-to-be-displayed
    # loop each produced chart
      # assign chart column value
  # display/save resulting chart
*/

public class Main {

  static TimeField timeField;
  static Path path;

  public static void main(String[] args) throws Exception {
    if (!parseArgs(args)) return;
    Files.createDirectories(Paths.get("CHARTS"));
    
    TreeMap<Integer, int[]> monthValuesByYear= getFileCountByMonth(path, timeField);
    
    int[] totMonthValues = new int[12];
    for (Integer year : monthValuesByYear.keySet()) {
      int[] monthValues = monthValuesByYear.get(year);
      for (int i=0; i<monthValues.length; i++) totMonthValues[i] += monthValues[i];
      BufferedImage bi = getMonthValuesRepresentation(monthValues);
      ImageIO.write(bi, "PNG", Paths.get("CHARTS", String.format(
          "%04d.png",
          year
          )).toFile());
      //BufferedImageJFrame.showImage(Integer.toString(year), bi);
    }
    
    BufferedImage bi = getMonthValuesRepresentation(totMonthValues);
    ImageIO.write(bi, "PNG", Paths.get("CHARTS", "total.png").toFile());
    
  }
  
  private static boolean parseArgs(String[] args) {
    
    if (args.length < 2) {
      printUsage();
      return false;
    }

    switch (args[0].toLowerCase()) {
    case "lastmodifiedtime":
      timeField = TimeField.LASTMODIFIEDTIME;
      break;
    case "creationtime":
      timeField = TimeField.CREATIONTIME;
      break;
    default:
      printUsage();
      return false;
    }
    
    path = new File(args[1]).toPath();
    return true;
  }

  private static void printUsage() {
    System.out.println("<lastModifiedTime | creationTime> <path>");
  }

  private static BufferedImage getMonthValuesRepresentation(int[] monthValues) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();

    dataset.addValue(monthValues[0], "", "1.");
    dataset.addValue(monthValues[1], "", "2.");
    dataset.addValue(monthValues[2], "", "3.");
    dataset.addValue(monthValues[3], "", "4.");
    dataset.addValue(monthValues[4], "", "5.");
    dataset.addValue(monthValues[5], "", "6.");
    dataset.addValue(monthValues[6], "", "7.");
    dataset.addValue(monthValues[7], "", "8.");
    dataset.addValue(monthValues[8], "", "9.");
    dataset.addValue(monthValues[9], "", "10.");
    dataset.addValue(monthValues[10], "", "11.");
    dataset.addValue(monthValues[11], "", "12.");
    
    JFreeChart chart = ChartFactory.createBarChart(
        null, null, null, dataset, PlotOrientation.VERTICAL, false, false, false);
    
    return chart.createBufferedImage(500, 200);
  }

  public static TreeMap<Integer, int[]> getFileCountByMonth(Path startPath, TimeField useTimeField) throws Exception {  
    SimplyPathCollector v = new SimplyPathCollector();
    Files.walkFileTree(startPath, v);
    
    TreeMap<Integer, int[]> monthValuesByYear = new TreeMap<>(new Comparator<Integer>() {
      @Override
      public int compare(Integer o1, Integer o2) {
        if (o1 < o2) return -1;
        if (o1 > o2) return 1;
        return 0;
      }
    });
    
    for (PathAndAttrs tuple : v.paths) {
      DateTime time;
      if (useTimeField == TimeField.CREATIONTIME) {
        time = new DateTime(tuple.attrs.creationTime().toMillis());
      } else {
        time = new DateTime(tuple.attrs.lastModifiedTime().toMillis());
      }
      
      ädd(monthValuesByYear, time.getYear(), time.getMonthOfYear());
    }
    
    return monthValuesByYear;
  }

  private static void ädd(TreeMap<Integer, int[]> monthValuesByYear, int year, int monthOfYear) {
    int[] monthValues;
    
    monthValues = monthValuesByYear.get(year);
    if (monthValues == null) {
      monthValuesByYear.put(year, new int[12]);
    }
    
    monthValues = monthValuesByYear.get(year);
    monthValues[monthOfYear-1]++;
  }

}
